jhwebtest

介绍

  上海精函信息科技有限公司前端上机测试题

软件架构

  基础代码采用vue编码。可自行引用其他框架。

Swagger-UI接口地址：
  http://47.75.136.30:9090/swagger-ui.html#/test-controller

业务需求

  一、注册 
      1. 自行画出注册页面，样式自定义；
      2. 账号为手机号码，校验手机号码正则；
      3. 密码长度为6-18的数字及字母，并进行正则校验，同时使用MD5加密；
      4. 调用注册接口，获取token后跳转至登录页面;

  二、登录
      1. 自行画出登录页面，样式自定义；
      2. 账号密码合法性判断,密码MD5加密；
      3. 调用登录接口，获取token，登录成功后跳转至文件上传页面;

  三、上传文件
      1. 自行画出文件上传页面，样式自定义；
      2. 文件格式校验（jpg格式）；
      3. 文件携带token上传，上传成功后提示成功；

  四、登出
      1. 携带token 调用后台登出接口，成功后删除存储的token，并返回登陆页面；